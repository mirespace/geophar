#! /bin/sh

version=$2
symlinkToUpstreamTarball=$3


newdir=$(echo $symlinkToUpstreamTarball | sed -e 's%^\.\./%%' -e 's/.orig.tar.gz//' -e 's/_/-/')
symlinkToUpstreamTarball=$(echo $symlinkToUpstreamTarball | sed -e 's%^\.\./%%')
newdir=${newdir}+dfsg1

echo "convert geophar-$version to $newdir"
wd=$(pwd)
cd ..
tar xzf $symlinkToUpstreamTarball
mv geophar-$version $newdir

removedFiles=""

for f in jsMath jquery.js underscore.js sympy 7zS.sfx; do
    for found in $(find $newdir -name $f); do
	removedFiles="$removedFiles $found"
    done
    echo "files to remove: $removedFiles"
    find $newdir -name $f | xargs rm -rf
done

# build the documentation, which requires dbus running and an X11 server
# these prerequisites are hard to meet inside Debian's compile farm.

wd1=$(pwd)
mkdir -p $newdir/doc; cd $newdir/doc; make html;
cd $wd1

echo "Those files were removed to comply with DFSG:" > $newdir/README.dfsg.txt
echo $removedFiles >> $newdir/README.dfsg.txt

archive=geophar_$version+dfsg1.orig.tar.xz

tar cJf $archive $newdir

rm -rf $newdir

echo created $archive

rm $(readlink $symlinkToUpstreamTarball)
rm $symlinkToUpstreamTarball
